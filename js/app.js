const obtenerData = async () => {
  try {
    const resp = await fetch("./data.json");
    const data = await resp.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

obtenerData().then((resp) => {
  mostrarData(resp);
});

const mostrarData = (data) => {
  const { squadName, homeTown, formed, members } = data;
  crearEncabezado(squadName, homeTown, formed);
  crearCards(members);
};

const crearEncabezado = (squadName, homeTown, formed) => {
  const titulo = document.querySelector("#title");
  const subtitulo = document.querySelector("#subtitle");
  titulo.innerHTML = `<p>${squadName}</p>`;
  subtitulo.innerHTML = `<p><strong>Hometown:</strong> ${homeTown} | <strong>Formed:</strong>: ${formed}</p>`;
};

const crearCards = (members) => {
  const container = document.querySelector("#container");
  members.forEach((member) => {
    const { name, secretIdentity, age, powers } = member;
    const card = document.createElement("div");
    card.innerHTML = `
    <h2>${name}</h2>
    <p><strong>Secret Identity:</strong> ${secretIdentity}</p>
    <p><strong>Age:</strong> ${age}</p>
    <p><strong>Superpowers:</strong></p>
    `;
    card.appendChild(listaDePoderes(powers));
    container.appendChild(card);
  });
};

const listaDePoderes = (powers) => {
  const lista = document.createElement("ul");
  powers.map((power) => {
    const li = document.createElement("li");
    li.innerHTML = `${power}`;
    lista.appendChild(li);
  });
  return lista;
};
